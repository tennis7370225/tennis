
import random

class Player:
    def __init__(self, name):
        self.name = name
        self.points = 0
        self.games = 0
        self.sets = 0

def point_winner(player1, player2):
    return random.choice([player1, player2])

def update_score(winner, loser):
    if winner.points == 40:
        if loser.points == 40:
            winner.points = "Advantage"
        elif winner.points == "Advantage":
            winner.games += 1
            winner.points = 0
            loser.points = 0
        else:
            winner.games += 1
            winner.points = 0
            loser.points = 0
    elif winner.points == "Advantage":
        winner.games += 1
        winner.points = 0
        loser.points = 0
    elif winner.points == 30:
        winner.points = 40
    elif winner.points == 15:
        winner.points = 30
    else:
        winner.points = 15

def check_set_winner(player1, player2):
    if player1.games >= 6 and player1.games - player2.games >= 2:
        player1.sets += 1
        player1.games = 0
        player2.games = 0
    elif player2.games >= 6 and player2.games - player1.games >= 2:
        player2.sets += 1
        player2.games = 0
        player1.games = 0

def check_match_winner(player1, player2):
    return player1.sets == 2 or player2.sets == 2

def display_score(player1, player2):
    print(f"{player1.name}: {player1.sets} sets, {player1.games} games, {player1.points} points")
    print(f"{player2.name}: {player2.sets} sets, {player2.games} games, {player2.points} points")

def play_game(player1, player2):
    while True:
        winner = point_winner(player1, player2)
        loser = player1 if winner is player2 else player2
        update_score(winner, loser)
        check_set_winner(player1, player2)
        display_score(player1, player2)
        if check_match_winner(player1, player2):
            break

    match_winner = player1 if player1.sets == 2 else player2
    print(f"\n{match_winner.name} wins the match!")

# Create players
player1 = Player("Player 1")
player2 = Player("Player 2")

# Play the game
play_game(player1, player2)
